<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get("/ktp", 'KtpOnlineController@index');
Route::get("/input_penduduk", 'KtpOnlineController@add');
Route::get("/edit/{id}", 'KtpOnlineController@edit');
Route::get("/delete/{id}", "KtpOnlineController@delete");

Route::post("/insert", "KtpOnlineController@insert");
Route::post("/update", "KtpOnlineController@update");
// Localhost/ktp/public/