<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendudukTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penduduk', function (Blueprint $table) 
        {
            $table->increments("id");
            $table->string("nik", 16)->nullable();
            $table->string("nama", 100);
            $table->string("jenis_kelamin", 1);
            $table->string("tempat_lahir", 50);
            $table->date("tgl_lahir");
            $table->text("alamat");
            $table->string("rt_rw", 10);
            $table->string("kelurahan", 50);
            $table->string("kecamatan", 50);
            $table->string("agama", 50);
            $table->string("status_perkawinan", 50);
            $table->string("pekerjaan", 50);
            $table->string("kewarganegaraan", 50);
            $table->string("berlaku_hingga", 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penduduk');
    }
}
