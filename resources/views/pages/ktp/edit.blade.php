@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Daftar Kependudukan</div>

                <div class="panel-body">
                    <form method="post" action="{{ url('update')}}">
                        {!! csrf_field() !!}
                        <input type="hidden" name="id" value="{{ $massa->id }}">

                        <div class="form-group">
                            <label>NIK</label>
                            <input type="text" name="nik" class="form-control" value="{{ $massa->nik }}">
                        </div>
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" name="nama" class="form-control" value="{{ $massa->nama }}">
                        </div>
                        <div class="form-group">
                            <label>Jenis Kelamin</label><br>
                            <input type="radio" name="jenis_kelamin" value="L">Laki-Laki<br>
                            <input type="radio" name="jenis_kelamin" value="P">Perempuan
                        </div>
                        <div class="form-group">
                            <label>Tempat Lahir</label>
                            <input type="text" name="tempat_lahir" class="form-control" value="{{ $massa->tempat_lahir }}">
                        </div>
                        <div class="form-group">
                            <label>Tanggal Lahir</label>
                            <input type="text" name="tgl_lahir" class="form-control" value="{{ $massa->tgl_lahir }}">
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" name="alamat" class="form-control" value="{{ $massa->alamat }}">
                        </div>
                        <div class="form-group">
                            <label>RT/RW</label>
                            <input type="text" name="rt_rw" class="form-control" value="{{ $massa->rt_rw }}">
                        </div>
                        <div class="form-group">
                            <label>Kelurahan</label>
                            <input type="text" name="kelurahan" class="form-control" value="{{ $massa->kelurahan }}">
                        <div class="form-group">
                            <label>Kecamatan</label>
                            <input type="text" name="kecamatan" class="form-control" value="{{ $massa->kecamatan }}">
                        </div>
                        <div class="form-group">
                            <label>Agama</label>
                            <select name="agama" class="form-control">
                                <option value="islam">Islam</option>
                                <option value="kristen">Kristen</option>
                                <option value="budha">Budha</option>
                                <option value="hindu">Hindu</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Status Perkawinan</label>
                            <input type="text" name="status_perkawinan" class="form-control">
                        <div class="form-group" value="{{ $massa->status_perkawinan }}">
                            <label>Pekerjaan</label>
                            <input type="text" name="pekerjaan" class="form-control" value="{{ $massa->pekerjaan }}">
                        <div class="form-group">
                            <label>Kewarganegaraan</label>
                            <input type="text" name="kewarganegaraan" class="form-control" value="{{ $massa->kewarganegaraan }}">
                        </div>
                        <div class="form-group">
                            <label>Berlaku Hingga</label>
                            <input type="text" name="berlaku_hingga" class="form-control" value="{{ $massa->berlaku_hingga }}">
                        </div>
                        <div class="form-group">
                            <input type="submit" name="" class="btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
