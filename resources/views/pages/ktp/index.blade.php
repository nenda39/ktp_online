@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Daftar Kependudukan</div>

                <div class="panel-body">
                    <a href="{{ url('input_penduduk')}}">Input Penduduk</a><br>
                    <table class="table table-stripped table-bordered">
                        <tr>
                            <th>NIP</th>
                            <th>Nama</th>
                            <th>Tempat, Tanggal Lahir</th>
                            <th>Alamat</th>
                            <th>Agama</th>
                            <th>Status</th>
                            <th>Pekerjaan</th>
                            <th>Kewarganegaraan</th>
                            <th>Masa Berlaku</th>
                            <th>Aksi</th>
                        </tr>
                        @foreach($massa as $m)
                        <tr>
                            <td>{{ $m->nik }}</td>
                            <td>{{ $m->nama }}</td>
                            <td>{{ $m->tempat_lahir }}, {{ $m->tgl_lahir }}</td>
                            <td>{{ $m->alamat }}</td>
                            <td>{{ $m->agama }}</td>
                            <td>{{ $m->status_perkawinan }}</td>
                            <td>{{ $m->pekerjaan }}</td>
                            <td>{{ $m->kewarganegaraan }}</td>
                            <td>{{ $m->berlaku_hingga }}</td>
                            <td>
                                <a href="{{ url('edit')}}/{{ $m->id}}">Edit</a>&nbsp;<a href="{{ url('delete')}}/{{ $m->id}}">Hapus</a></td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
